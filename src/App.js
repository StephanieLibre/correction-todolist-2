import React, {useState} from 'react';
import './App.css';

function App() {
  // Variables react
  const [tasks, setTasks] = useState([])
  const [currentTask, setCurrentTask] = useState("")
  const [displayDones, setDisplayDones] = useState(true)

  // Fonctions "metier"
  function addTask(event) {
    event.preventDefault()
    if (currentTask !== "") {
      const newTask = {
        label: currentTask,
        done: false
      }
      setTasks([...tasks,  newTask])
      setCurrentTask("")
    }
  }

  function handleTaskChange(event) {
    setCurrentTask(event.target.value)
  }

  function toggleDone(task) {
    return function () {
      task.done = !task.done
      setTasks([...tasks])
    }
  }

  function handleChangeDisplay(event) {
    setDisplayDones(event.target.checked)
  }

  // fonctions d'affichage

  function displayTasks() {
    return tasks.filter(tasks => !tasks.done || displayDones).map((task, i) => <li key={i} onClick={toggleDone(task)} className={task.done?"done":""}>{task.label}</li>)
  }

  return (
    <div className="App">
      <label><input type="checkbox"  checked={displayDones} onChange={handleChangeDisplay} /> Accficher les taches réalisées</label>
      <form onSubmit={addTask}>
        <input type="text" placeholder="nouvelle tâche" value={currentTask} onChange={handleTaskChange}/>
        <input type="submit" value="ajouter"/>
      </form>
      <ul>
        {displayTasks()}
      </ul>
    </div>
  );
}

export default App;
